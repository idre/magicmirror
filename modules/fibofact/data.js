var data = {
	leader: {
		text: ["Тэр өлсөхөөөрөө их ууртай!", "Гэхдээ хавирга шарахдаа маш сайн!", "Тэр дандаа бусдыгаа уриалан манлайлдаг.", "Яагаад гэвэл тэр Аддарчуудын ангийн дарга"],
		image: "https://blog.vantagecircle.com/content/images/2020/02/Team_leader_skills.png"
	},
	two: {
		text: ["Эхнэр нөхөр хоёр гээд ангийнханаасаа нээх тасарч зугтааж гүйгээд байсан юм  бол байхгүй.", "Электрон бараа, шинжлэх ухаан, технологийн талаар яриад эхэлбэл онцгойрно чиг харин"],
		image: "https://ds393qgzrxwzn.cloudfront.net/resize/m600x500/cat1/img/images/0/B5wU0l6s8j.jpg"
	},
	jewel: {
		text: ["He doesn’t speak much.", "But he believes that his work should talk more than what he says.", "He knows how to enjoy"],
		image: "https://images.unsplash.com/photo-1507099985932-87a4520ed1d5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
	},
	sister: {
		text: [
			"Хүмүүс түүнтэй хамт байх дуртай.",
			"Тэр бусдыг элгийг нь хөштөл инээлгэдэг.",
			"",
			"Тэр төрөлхийн авьяаслаг шүлэгч, яруу найрагч",
			"Гэхдээ зөвхөн 3 мөрт Хайку шүлгээр төрөлжсөн.",
			"",
			"Тэр бусдад баяр баясгалан бэлэглэгч.",
			"",
			"Тэр Аддарынх"
		],
		image: "https://images.theconversation.com/files/290476/original/file-20190902-175673-1103xqc.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=1200&h=900.0&fit=crop"
	},
	flower: {
		text: ["Тэр хөөрхөн, эгдүүтэй", "Тэр бас гоё дуулна.", "Гэхдээ санхүү тоо бүртгэл гээд ярьвал хөөрхөн цаашаа айлгана л гэж мэд!"],
		image: "https://target.scene7.com/is/image/Target/GUEST_1fc9b040-282d-440d-a19c-66c6768cc12e?wid=700&hei=700&qlt=80&fmt=webp"
	},
	woman: {
		text: ["She is cool.", "She is smart.", "", "Тэр Аддарынх", "Be like Энхмэнд"],
		image: "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Gal_Gadot_by_Gage_Skidmore_2.jpg/800px-Gal_Gadot_by_Gage_Skidmore_2.jpg"
	},
	battulga: {
		text: ["BATTULGA Khaltmaa, President of Mongolia"],
		image: "https://president.mn/en/wp-content/uploads/2018/03/PresidentSmall.jpg"
	},
	trump: {
		text: ["Donald Trump, President of US"],
		image: "https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Donald_Trump_official_portrait.jpg/1200px-Donald_Trump_official_portrait.jpg"
	}
};

var keywords = ["leader", "two", "jewel", "sister", "flower", "woman", "battulga", "trump"];
