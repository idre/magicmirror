/* eslint-disable no-undef */
Module.register("fibofact", {
	// Default module config.
	defaults: {
		text: "Hello Fibo Fact Alexa!"
	},
	// Define required scripts.
	getScripts: function () {
		return ["data.js"];
	},
	// Define styles.
	getStyles: function () {
		return ["fibofact.css"];
	},
	// Override dom generator.
	getDom: function () {
		function replaceAll(str, find, replace) {
			return str.replace(new RegExp(find, "g"), replace);
		}
		var wrapper = document.createElement("div");
		wrapper.className = "fibo_fact_container";
		var socket = io("http://52.204.6.157:8000");
		socket.on("connect", function () {
			Log.log("Uuree socket connected");
		});
		socket.on("get_image", (result) => {
			wrapper.innerHTML = "";
			var innerwrapper = document.createElement("div");
			innerwrapper.className = "fibo_inner_container";
			wrapper.appendChild(innerwrapper);
			if (keywords.includes(result.image_name)) {
				innerwrapper.innerHTML = "";
				var image = document.createElement("img");
				image.className = "fibo_fact_image";
				image.setAttribute("src", data[result.image_name].image);
				innerwrapper.appendChild(image);
				data[result.image_name].text.map((tex) => {
					var description = document.createElement("div");
					description.innerHTML = tex;
					innerwrapper.appendChild(description);
					return;
				});
			} else {
				innerwrapper.innerHTML = result.image_name;
			}
		});
		socket.on("loading", () => {
			wrapper.innerHTML = "";
			var innerwrapper = document.createElement("div");
			innerwrapper.className = "fibo_inner_container";
			wrapper.appendChild(innerwrapper);
			innerwrapper.innerHTML = "Уншиж байна ...";
		});
		socket.on("exchange", (result) => {
			wrapper.innerHTML = "";
			var innerwrapper = document.createElement("div");
			innerwrapper.className = "fibo_inner_container";
			wrapper.appendChild(innerwrapper);
			innerwrapper.innerHTML = replaceAll(result.html, 'aria-describedby="dt_basic_info"', 'border="1"');
		});
		socket.on("airport", (result) => {
			wrapper.innerHTML = "";
			var innerwrapper = document.createElement("div");
			innerwrapper.className = "fibo_inner_container";
			wrapper.appendChild(innerwrapper);
			var final = replaceAll(result.html, 'align="center"', 'class="airport"');
			innerwrapper.innerHTML = replaceAll(final, "/media/logo/", "http://airport.gov.mn/media/logo/");
		});
		socket.on("close", () => {
			wrapper.innerHTML = "";
		});
		socket.on("disconnect", function () {
			Log.log("Uuree socket disconnected");
		});
		return wrapper;
	}
});
